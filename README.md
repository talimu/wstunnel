

To try it:

1. Login to Okteto, and add a secret called PW with your password.

![setting](https://i.imgur.com/TEkIkpH.png)

2. Click [here](https://cloud.okteto.com/deploy?repository=https://bitbucket.org/talimu/wstunnel) to deploy wstunnel